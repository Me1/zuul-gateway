package com.ts.zuul.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by Cefis on 2018/9/5.
 */
@RestController
@EnableEurekaClient
@SpringBootApplication
public class ServerApplication {


    @RequestMapping(value = "/available")
    public String available(){
        System.out.println("Spring1 in Action");
        return "Spring1 in Action";
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String save(@RequestBody Map<String,Object> map){
        System.out.println("Spring1 in Action");
        return "Spring1 in Action";
    }


    @RequestMapping(value = "/check-out")
    public String checkOut(){
        return "Spring1 in Action";
    }

    public static void main(String[] args){
        SpringApplication.run(ServerApplication.class,args);
    }

}
