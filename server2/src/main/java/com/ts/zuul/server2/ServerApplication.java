package com.ts.zuul.server2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Cefis on 2018/9/5.
 */
@RestController
@SpringBootApplication
public class ServerApplication {


    @RequestMapping(value = "/available")
    public String available(){
        System.out.println("Spring2 in Action");
        return "Spring2 in Action";
    }

    @RequestMapping(value = "/check-out")
    public String checkOut(){
        return "Spring2 in Action";
    }

    public static void main(String[] args){
        SpringApplication.run(ServerApplication.class,args);
    }

}
