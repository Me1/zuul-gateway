package com.ts.zuul.gateway;

import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.repository.DefaultRateLimiterErrorHandler;
import com.marcosbarbero.cloud.autoconfigure.zuul.ratelimit.config.repository.RateLimiterErrorHandler;
import com.ts.zuul.gateway.filter.VerifyFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 * Created by Cefis on 2018/9/5.
 */
@EnableZuulProxy
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args){
        SpringApplication.run(GatewayApplication.class,args);
    }


    /**
     * 注册过滤器
     */
    @Bean
    public VerifyFilter verifyFilter(){
        return new VerifyFilter();
    }


    @Bean
    public RateLimiterErrorHandler rateLimitErrorHandler() {
        return new DefaultRateLimiterErrorHandler() {
            @Override
            public void handleSaveError(String key, Exception e) {
                System.out.println(111);
                // custom code
            }

            @Override
            public void handleFetchError(String key, Exception e) {
                System.out.println(222);
                // custom code
            }

            @Override
            public void handleError(String msg, Exception e) {
                System.out.println(333);
                // custom code
            }
        };
    }
}
