package com.ts.zuul.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import io.jmnarloch.spring.cloud.ribbon.support.RibbonFilterContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by Zhou_Bing on 2018/9/7.
 */
@Component
public class ABTestFilter extends ZuulFilter{
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        if("A".equals(ctx.getRequest().getParameter("type"))){
            RibbonFilterContextHolder.getCurrentContext().add("launch","1");
        }else{
            RibbonFilterContextHolder.getCurrentContext().add("launch","2");
        }

        return null;
    }
}
