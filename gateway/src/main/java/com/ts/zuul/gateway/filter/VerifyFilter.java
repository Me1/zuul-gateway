package com.ts.zuul.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Zhou_Bing on 2018/9/5.
 */
public class VerifyFilter extends ZuulFilter{

    @Override
    public Object run() {

        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        String signature = request.getParameter("signature");
        if(signature==null){
            //过滤该请求，不往下级服务去转发请求，到此结束
            ctx.setSendZuulResponse(false);//决定是否往最终的服务转发请求
            ctx.setResponseStatusCode(401);
            ctx.setResponseBody("{\"result\":\"signature is empty!\"}");
            return null;
        }
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;// 是否执行该过滤器，此处为true，说明需要过滤
    }


    @Override
    public String filterType() {
        /**
         * pre：可以在请求被路由之前调用
         * route：在路由请求时候被调用
         * post：在route和error过滤器之后被调用
         * error：处理请求时发生错误时被调用
         */
        return "pre";// 前置过滤器
    }

    @Override
    public int filterOrder() {
        return 10;// 优先级为0，数字越大，优先级越低
    }




}
